#include <stdio.h>
#include <stdint.h>
#include <arpa/inet.h>

uint32_t readAndReturnData(char* fileName) {
	uint32_t fileData;
	FILE *file = fopen(fileName, "rb");
	if(file == NULL) {
		printf("\n");
		return 0;
	}
	fread(&fileData, sizeof(fileData), 4, file);
	fclose(file);
	return htonl(fileData);
}

int main(int argc, char* argv[]) {
	if (argc != 3)
		return 0;
	uint32_t number1 = readAndReturnData(argv[1]);
	uint32_t number2 = readAndReturnData(argv[2]);
	printf("%d(0x%x) + %d(0x%x) = %d(0x%x)", number1, number1, number2, number2, number1+number2, number1+number2);
	return 0;

}
