TARGET = add-nbo

$(TARGET): add-nbo.cpp
	g++ -o $(TARGET) add-nbo.cpp

clean:
	rm -f $(TARGET)
